﻿module AuthService
open FSharp.Data
open System.Collections.Generic
open System.Linq;
type DBUser = {Name : string; PasswordHash : string; Email : string; Id : int}


[<Literal>]
let private sqlQuery = "Select Name, PasswordHash, Email, Id  from Users where Name = @name and PasswordHash = @passw"
[<Literal>]
let private insertSql = "Insert into Users (Id, Name, Email, PasswordHash) values (2, @name, @email, @passw)"

let private dbConnectString = "Server=(localdb)\\MSSQLLocalDB;Database=Auri.DB;Trusted_Connection=True;"

let getUser name passw = async{
    let command = new SqlCommandProvider<sqlQuery, "Server=(localdb)\\MSSQLLocalDB;Database=Auri.DB;Trusted_Connection=True;">(dbConnectString)
    let! asyncRes = command.AsyncExecute(name, passw)
    let user = asyncRes.FirstOrDefault()
    match user with 
        | null -> return  None
        | x -> return Some({Name = x.Name; PasswordHash = x.PasswordHash; Email = x.Email; Id = x.Id})
    }

    