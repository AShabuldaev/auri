﻿namespace Api.Auth.FS.Controllers
open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open AuthService
[<ApiController>]
[<Route("[controller]")>]
type AuthController (logger : ILogger<AuthController>) =
    inherit ControllerBase()
    let summaries = [| "Freezing"; "Bracing"; "Chilly"; "Cool"; "Mild"; "пизда"; "член"; "говно"; "жопа"; "залупа" |]

    [<HttpGet>]
    member __.Get() = async{
    let! res = getUser "Alex" "123"
    match res with
         |Some(x) -> return JsonResult(x)
         |None    -> return JsonResult("Not found")
    }
       